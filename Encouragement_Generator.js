// Encouragement Generator v0.1b

// Copyright 2021 Robert Paul AKA MxVoid, Wondermundo,
// Empress Trash, MxSpite, and the #FamJam NFT community

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//    http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// ***Global variables***

// Default background color (dark gray)
let bgColor = '#111111';

// Font variables
let labelfont;
let labelsize = 14;
let regFont;
let hypeFont;
let zomgFont;
let wordsize = 36;
let fontColor = '#cccccc';

// Sleep delay (in milliseconds)
const MILLIS = 3000;

// Drop-down menu selector
let sel;

// Customized To: and From: messages
let customized = false;
let customizedMessage = "To: [name or handle]\t\t\tFrom: [name or handle]";

// "Down-to-earth encouragement" phrases
let regEncourage = ["Your work is valuable.",
  "It takes time. And that's OK.",
  "You are worthy.",
  "You are not alone.",
  "Be authentic. Only you can be you.",
  "You are enough.",
  "Cultivate and practice gratitude.",
  "Comparison is the thief of joy.",
  "Asking for help is a sign of strength, not weakness.",
  "Stop and breathe.",
  "There's plenty of space for you.",
  "Know thyself.",
  "Listen to your body.",
  "Your feelings are valid.",
  "Go and do.",
  "Everyone else is trying to figure it out, too."];

// "Warm encouragement" phrases
let warmEncourage = ["Your work is good!",
  "Keep going!",
  "Your voice is unique and important.",
  "You're so much stronger than you realize.",
  "You have a whole community behind you.",
  "Trust yourself.",
  "You can do this.",
  "Take all the time you need for self-care.",
  "You don't have to pick a destination, just a direction.",
  "You have so much to offer.",
  "You deserve kindness; be kind to youself."];

// "Strong hype" phrases
let strongHype = ["Your work is amazing!",
  "What you do is so inspiring!",
  "We all love what you do!",
  "You're awesome!",
  "You make the space better by being in it!",
  "We're so proud of you!",
  "Our world needs you and people like you!",
  "You are beautiful!",
  "WAGMI!"];

// "Downright sycophantic" phrases
let buttKissingHype = ["Your work is absolutely mind-blowing!",
  "Your work is ground-breaking!",
  "You're a genius!",
  "You're casting a truly historic legacy!",
  "Your brilliance is unprecedented!",
  "You're a visionary!",
  "You're far ahead of your time!",
  "You are a rock star!"];

// "ZOMG TOTES HYPERBOLIC!!!" phrases
let zomgSuperHype = ["YOUR WORK IS THE BEST EVER!!!1",
  "YOU ARE THE BEST IN THE HISTORY OF FOREVER!!",
  "WOW!!! YOU ARE A DIVINE BEING!!!",
  "POSITIVE VIBES ONLY!!!!1",
  "YOU ARE ABSOLUTELY LEGENDARY!!!!1",
  "YOU ARE TOTES THE GOAT!!!!",
  "YOUR WORK IS EARTH-SHATTERING!!!"];

// ***Initialization***

function preload() {
  // Ensure the .ttf or .otf fonts stored in the assets directory
  // are loaded before setup() and drawWords() are called
  labelfont = loadFont('data/CourierPrime-Regular.ttf');
  regfont = loadFont('data/OpenSans-SemiBold.ttf');
  hypefont = loadFont('data/OpenSans-ExtraBoldItalic.ttf');
  zomgfont = loadFont('data/PollerOne-Regular.ttf');
}

function setup() {
  // load canvas
  createCanvas(window.innerWidth, window.innerHeight);
  
  // load and set fonts
  textAlign(CENTER);
  //textWrap(WORD);
  
  // initialize drop-down menu
  sel = createSelect();
  // Set drop-down menu HTML styles
  sel.style('background-color', bgColor);
  sel.style('color', fontColor);
  sel.style('font-size', labelsize);
  sel.style('font-family', 'Courier Prime');
  // Place drop-down menu on canvas
  sel.position(10, 30);
  // Set drop-down menu options
  sel.option('Regular encouragement', 1);
  sel.option('Warm encouragement', 2);
  sel.option('Strong hype', 3);
  sel.option('Downright sycophantic', 4);
  sel.option('ZOMG TOTES HYPERBOLIC!!!', 5);
  // Select initial hype option
  sel.selected(1);
  
  // Infinite loop; constantly draw
  drawWords();
}

// ***Drawing function***
// Because we use Promise/await/async to implement a sleep timer, we can't
// use the Processing/p5.js draw() function to draw the output

async function drawWords() {
  // TODO: Info panel
  // TODO: Automatic window resizing
  
  // output string
  let s;
  
  // Hype mode variable
  let phraseMode = sel.value();
  
  // Previous phraseMode
  let prevMode;
  
  // Toxic positivity warning state
  let warningState = true;
  
  // Start the infinite loop
  while(true) {
    // draw background and label text
    background(bgColor);
    fill(fontColor);
    textSize(labelsize);
    textFont(labelfont);
    text("Encouragement level:", 95, 20);
    
    if (customized) {
      text(customizedMessage, width/2, 20);
    }
    
    // Save the previous hype mode
    prevMode = phraseMode;
    // get hype mode from drop-down menu
    phraseMode = sel.value();
    
    // TODO: Special text effects
    
    // select output according to selected hype mode
    switch(phraseMode) {
      case '1': // Regular encouragement
        s = random(regEncourage);
        textFont(regfont);
        break;
      
      case '2': // Warm encouragement
        s = random(warmEncourage);
        textFont(regfont);
        break;
      
      case '3': // Strong hype
        s = random(strongHype);
        textFont(hypefont);
        break;
      
      case '4': // Downright sycophantic
        s = random(buttKissingHype);
        textFont(hypefont);
        break;
      
      case '5': // ZOMG TOTES HYPERBOLIC!!!
        // Show toxic positivity warning
        if (warningState)
        {
          // Pull up dialog box with warning prompt and negate return value
          // OK => warningState set to false
          // Cancel => warningState remains true
          warningState = !(warningBox());
          
          // If the user canceled, set the selection menu to the previous hype mode
          if (warningState) {
            sel.value(prevMode);
            if (prevMode < 3) {
              textFont(regfont);
            }
            else {
              textFont(hypefont);
            }
          }
          else { // User accepted mode
            s = random(zomgSuperHype);
            textFont(zomgfont);
          }
        }
        else { // User previously accepted this mode
          s = random(zomgSuperHype);
          textFont(zomgfont);
        }
        break;
      
      default: // Catch out-of-range values (from cosmic rays, cursed items, etc.)
        break;
    }
    
    // Set the text values
    textSize(wordsize);
    // Draw the text to screen
    text(s, width/12, height/2.5, width/1.2, height);
    
    // Sleep for MILLIS milliseconds
    await sleep(MILLIS);
    // Clear the draw screen after sleep time
    clear();
  }
}

// Toxic positivity warning
function warningBox() 
{
  return confirm("WARNING!\nThis mode is full of toxic positivity.\nUse with caution!\nSelect \"OK\" to continue:");
}

// Sleep function
function sleep(millisecondsDuration)
{
  return new Promise((resolve) => {
    setTimeout(resolve, millisecondsDuration);
  });
}
